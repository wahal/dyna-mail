#!/usr/bin/env python

#-------------------------------------------------------------------#
# Project : Email Using smtplib                                     #
# Script  : Sending & Recieving End                                 #
# Author  : Mrinal Wahal                                            #
#-------------------------------------------------------------------#

import smtplib, imaplib, email, getpass, os, time, sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.application import MIMEApplication
from email.mime.audio import MIMEAudio
from email.parser import HeaderParser

print "-"*60
print "Dyna E-Mail | By Mrinal Wahal"
print "-"*60

def sleep():
    print
    for second in range(5,0,-1):
        print "[+] Exiting In %s." % second
        time.sleep(1)
    print "\n[*] See Ya, Grasshopper !"
    sys.exit()    

def ask_attachment():
    
    query = raw_input("\n[+] Do You Have An Attachment ? (Y/N): ")
    if query == "y" or query == "Y":
        print """

What's The File Type ?
            
1) Text
2) Image
3) Audio
4) Application File
            """
        file_type = raw_input("+] Type: ")
    
        destination = raw_input("\n[+] Destination (Along With Extension): ")
        if file_type == "1":
            attachment = MIMEText(open(destination,"r+").read())
        elif file_type == "2":
            attachment = MIMEImage(open(destination,"rb").read(), name = os.path.basename(destination))
        elif file_type == "3":
            attachment = MIMEAudio(open(destination,"rb").read(), name = os.path.basename(destination))
        elif file_type == "4":
            attachment = MIMEApplication(open(destination, "rb").read(), name = os.path.basename(destination))
        else:
            print "File Type Not Found. File Not Attached. "
            attachment = None
        message.attach(attachment)
    else:
        pass
        
def send_mail(service, email_id, password):
    
    if service == '1':
        smtp_server = "smtp.gmail.com:587"
    elif service == '2':
        smtp_server = "smtp.mail.yahoo.com:465"
    elif service == '3':
        smtp_server = "smtp.live.com:25"
    else:
        print "Alternative Not Found."
        sleep()
    
    from_name = email_id.split("@")
    username = from_name[0]
    
    server = smtplib.SMTP(smtp_server)
    print "\n[+] Server Successfully Established.\n"
    server.set_debuglevel(True)
    server.ehlo()
    
    if server.has_extn("STARTTLS"):
        print "\n[-] Server Requires TLS Encryption\n"
        server.starttls()
        print "\n[+] TLS Activated On The Server.\n"
        server.ehlo()
        print "\n[+] Successfully Recognized On The Encrypted Server."
    
    try:
        print "\n[+] Verifying Username: %s\n" % username
        server.verify(username)
        print "\n[+] Username Verified.\n"
    except smtplib.SMTPAuthenticationError:
        print "\n[-] Error: Username Is Invalid."
        server.quit()
        sleep()
        
    try:
        print "\n[+] Signing In.\n"
        server.login(email_id,password)
        print "\n[+] Access Gained Successfuly."
    except smtplib.SMTPException:
        print "\n[-] Error: Username Or Password Credentials Are Invalid."
        server.quit()
        sleep()
        
    to = raw_input("\n[+] Email To: ")
    toaddr = to.split(",")
    
    cc = raw_input("\n[+] Cc: ")
    ccaddr = cc.split(",")
        
    subject = raw_input("\n[+] Subject: ")
    body = raw_input("\n[+] Message: ")
    
    global message
    
    message = MIMEMultipart()
    message["From"]  = email_id
    message["To"] = ", ".join(toaddr)
    message["Cc"] = ", ".join(ccaddr)
    message["Subject"] = subject
    message.attach(MIMEText(body))
    ask_attachment()
    
    try:
        print
        server.sendmail(email_id,toaddr,message.as_string())
        print "\n[+] Email Sent.\n"
        server.quit()
        print "[+] Server Terminated."
        sleep()
    except smtplib.SMTPDataError:
        print "\n[-] Error: Server Refused To Accept The Message. Mail Delivery Failed."
        server.quit()
        sleep()

def read_mail(service, username, password):
    
    print "\n[+] Establishing Server."
    
    if service == 1:
        imap = imaplib.IMAP4_SSL("imap.gmail.com", 993)
    elif service == 2:
        imap = imaplib.IMAP4_SSL("imap.mail.yahoo.com", 993)
    elif service == 3:
        imap = imaplib.IMAP4_SSL("imap-mail.outlook.com", 993)
        
    print "\n[+] Server Successfully Established.\n"
    
    print "[-] Gaining Access On The Account."
    imap.login(username, password)
    print "\n[+] Access Gained On: ", username
    
    imap.select('INBOX')
    print "\n[+] Searching For Unread Mesaages."
    
    status, response = imap.search('INBOX', '(UNSEEN)')
    unread_msg_nums = response[0].split()
    new = len(unread_msg_nums)
    
    if new != 0:
        print "\n[+] Unread Messages: ", new
    else:
        print "\n No Unread Messages."
        sleep()
    
    resp, data = imap.FETCH(1, '(RFC822)')
        
    msg = HeaderParser().parsestr(data[0][1])
    mail = email.message_from_string(data[0][1])
         
    print "\nFrom: ", msg['From']
    print "To: ", msg['To']
    print "Subject: ", msg['Subject']
        
    for part in mail.walk():
    # multipart are just containers, so skip them
        if part.get_content_maintype() == 'multipart':
          continue
         
    # I'm interested only in the simple text messages
        if part.get_content_subtype() != 'plain':
            continue
         
        payload = part.get_payload()
        print payload
            
    sleep()

def main():
    print """

Choose Your Webservice:

1) Google Mail
2) Yahoo Mail
3) Outlook/ Hotmail
    """
    
    service = raw_input("[+] Webservice: ")
    
    practice_input = raw_input("\n[+] Do You Wish To Send/Read Email ? (S/R): ")
    practice = practice_input.lower()
    
    from_addr = raw_input("\n[+] Username: ")
    password = getpass.getpass("\n[+] Password: ")
    
    if practice == "s":
        send_mail(service, from_addr, password)
    elif practice == "r":
        read_mail(service, from_addr, password)
        
if __name__ == '__main__':
    main()